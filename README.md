# JFJClient

Small wrapper for [jungfraujoch_openapi_python](https://gitlab.psi.ch/jungfraujoch/jungfraujoch_openapi_python).

## Example Usage

```python
from jfjclient import JFJClient


jfj = JFJClient("http://sf-daq-2:5232")

jfj.acquire(
    beam_x_pxl = 1613,
    beam_y_pxl = 1666,
    detector_distance_mm = 151,
    photon_energy_keV = 12,
    sample_name = "test",
    file_prefix = "test",
    ntrigger = 10
)
```

